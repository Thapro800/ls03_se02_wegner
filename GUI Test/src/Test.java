import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class Test extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
					frame.setTitle("Alien-Defence");
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Level ausw\u00E4hlen\r\n");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(142, 53, 144, 39);
		getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Willkommen bei Alien-Defence!");
		lblNewLabel.setForeground(Color.GREEN);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(111, 11, 249, 31);
		getContentPane().add(lblNewLabel);
		
				
		JButton btnLevel = new JButton("Level 1");
		btnLevel.setBounds(50, 103, 102, 31);
		btnLevel.setVisible(false);
		btnNewButton.addActionListener(e -> btnLevel.setVisible(true));
		getContentPane().add(btnLevel);
		
		JButton btnNewButton_1 = new JButton("Level 2\r\n");
		btnNewButton_1.setBounds(172, 103, 102, 31);
		btnNewButton_1.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_1.setVisible(true));
		getContentPane().add(btnNewButton_1);
		
		JButton btnLevel_1 = new JButton("Level 3");
		btnLevel_1.setBounds(296, 103, 102, 31);
		btnLevel_1.setVisible(false);
		btnNewButton.addActionListener(e -> btnLevel_1.setVisible(true));
		getContentPane().add(btnLevel_1);
		
		JButton btnNewButton_2 = new JButton("Level 4");
		btnNewButton_2.setBounds(50, 145, 102, 31);
		btnNewButton_2.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_2.setVisible(true));
		getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Level 5");
		btnNewButton_3.setBounds(172, 145, 102, 31);
		btnNewButton_3.setVisible(false);
		btnNewButton.addActionListener( e -> btnNewButton_3.setVisible(true));
		getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Level 6");
		btnNewButton_4.setBounds(296, 145, 102, 31);
		btnNewButton_4.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_4.setVisible(true));
		getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Level 7");
		btnNewButton_5.setBounds(50, 187, 102, 31);
		btnNewButton_5.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_5.setVisible(true));
		getContentPane().add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Level 8");
		btnNewButton_6.setBounds(172, 187, 102, 31);
		btnNewButton_6.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_6.setVisible(true));
		getContentPane().add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Level 9");
		btnNewButton_7.setBounds(296, 187, 102, 31);
		btnNewButton_7.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_7.setVisible(true));
		getContentPane().add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("Level 10");
		btnNewButton_8.setVisible(false);
		btnNewButton.addActionListener(e -> btnNewButton_8.setVisible(true));
		btnNewButton_8.setBounds(50, 230, 102, 31);
		getContentPane().add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("Highscores");
		btnNewButton_9.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnNewButton_9.setBounds(296, 54, 102, 39);
		btnNewButton_9.addActionListener(e -> btnLevel.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_1.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnLevel_1.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_2.setVisible(false));
		btnNewButton_9.addActionListener( e -> btnNewButton_3.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_4.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_5.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_6.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_7.setVisible(false));
		btnNewButton_9.addActionListener(e -> btnNewButton_8.setVisible(false));
		getContentPane().add(btnNewButton_9);
	}
}
